function fizzbuzz(maxValue) {
  for (let counter = 1; counter <= maxValue; counter++) {
    if (counter % 2 === 0 && counter % 3 === 0) {
      console.log ("FizzBuzz, ")
    }
    else if (counter % 2 === 0) {
      console.log ("Fizz, ")
    }
    else if (counter % 3 === 0) {
      console.log ("Buzz, ")
    }
    else if (counter % 2 !== 0 && counter % 3 !== 0) {
      console.log (counter + ", ")    
    }
  }
}
console.log(fizzbuzz(20))